//We can initialize an empty array  two ways:

// let colors= [];
// let colors = new Array() //uncommon

// //Arrays can hold any type of data

// let random_collection = [49, true, "Hermione", null];

// //Arrays have a length property

// let nums = [45, 37, 89, 24];
// nums.length //4

// let movies = ["Godfather", "Mission Impossible", "Avengers", "Venom", "The Nun"];

// console.log(movies[0]); // Godfather
// console.log(movies[1]); // Mission Impossible
// console.log(movies[2]); // Avengers

// movies[1] = "Halloween";

// console.log(movies[1]); // Halloween
// console.log(movies.length); // length is 5
// console.log(movies.length-1); // length is 4

// let colors = ["red", "orange", "yellow"];
// console.log("> colors");
// console.log(colors);
// console.log(colors.push('green', 'blue'));
// console.log("> colors");
// console.log(colors);
// console.log(colors.push(()=>{return "violet";}));
// console.log("> colors");
// console.log(colors);
// console.log("> colors[5]");
// console.log(colors[5]);
// console.log("> colors[5]()");
// console.log(colors[5]());

// array.pop()

// let colors = ["red", "orange", "yellow"];

// colors.unshift("infrared");
// //["infrared", "red", "orange", "yellow"]

// console.log(colors);

// let tuitt = ["Charles", "Paul", "Sef", "Alex", "Paul"];

// //returns the first index at which a given element can be found
// console.log(tuitt.indexOf("Sef")); // 2

// //finds the first instance of Paul 1 not 4
// console.log(tuitt.indexOf("Paul")); // 1


// //returns the -1 if the element is not present.
// console.log(tuitt.indexOf("Hulk")); // -1

let colors = 
["red", "orange", "yellow", "green"];

// for(let i = 0; i < colors.length; i++){
// 	console.log(colors[i]);
// }

colors.forEach((color)=>{
	console.log(color);
});



// const scores = [
// 	[12, 15, 16],
// 	[11, 9, 8],
// 	[1, 5, 6],

// ]

// for(let i = 0; i < scores.length; i++){
// 	console.log(scores[i])
// 	// another loop for scores[i]
// 	for(let j = 0; j < scores[i].length; j++){
// 		console.log(scores[i][j])
// 	}
// }

// // OBJECTS
// const grades = [98, 87, 91, 84]

//  const myGrades = {
//  	// properties:
//  	lastName: 'Zamora',//key value pairs
//  	firstName: 'Rie',
//  	math: 87,
//  	english: 91,
//  	programming: 84,
//  	hobbies: ['coding', 'writing', 'cosplay']
//  }
//  // access through dot notation
//  console.log(myGrades.firstName)
//  //pag arrays index[0]
//  console.log(myGrades.hobbies[1])



 // objects may label, arrrays wala

 let person = {
 	firstName: 'John',
 	lastName: 'Smith',
 	email: ['jsmith@gmail.com', 'js@gmail.com'],
 	address: {
 		city: 'Tokyo',
 		country: 'Japan',
 	},
 	greeting: function(){
 		return 'hi';
 	}
 }


 const shoppingCart = [
 	{
 		product: 'shirt',
 		price: 99.90
 		qty: 2
 	},
 	{
 		product: 'watch',
 		price: 9.90
 		qty: 1},
 	{
 		product: 'laptop',
 		price: 99
 		9.90
 		qty: 2
 	},
 ]

 const blogger = {
 	lname: 'Smith'
 	fname: 'John'
 	posts: [
 		{
 			title: 'my first post'
 			body: 'lorem ipsum'
 			comments: [

 			]
 		},{},{}
 	]
 }