
// //Arrow Functions

// // function sum(num1,num2){
// // 	return num1+num2;
// // }

// // const sum2 = (num1,num2) => num1+num2

// // function isPositive(num){
// // 	return num >= 0;
// // }

// // const isPositive2 = num => num >= 0

// // function randNum(){
// // 	return Math.random();
// // }

// // const randNum2 = () => Math.random()

// /*
// Create an object to hold information on your favorite recipe. It should have properties for title (a string), servings (a number), and ingredients (an array of strings).

// feedback that prints in the console 'Delicious!'
// */

// const recipe = {
// 	title: "biko",
// 	servings: 8,
// 	ingredients: ['glutinous rice', 'brown sugar', 'coconut milk', 'salt'],

// 	feedback: () => console.log('Delicious!'),

// }


// //Create an array of objects, where each object describes a book and has properties for the title (a string), author (a string), and alreadyRead (a boolean indicating if you read it yet)./

// const books = [
// 	{
// 		title: 'Tuesdays with Morrie',
// 		author: 'Mitch Albom',
// 		alreadyRead: 'true',
// 	},
// 	{
// 		title: 'The Five People You Meet in Heaven',
// 		author: 'Mitch Albom',
// 		alreadyRead: 'false',
// 	},
// 	{
// 		title: 'The Secret',
// 		author: 'Rhonda Byrne',
// 		alreadyRead: 'true',
// 	}

// ]

// // const books = [
// // 	{
// // 		title: "AAA",
// // 		author: "BBB",
// // 		alreadyRead: true,
// // 		rating: 5
// // 	},
// // 	{
// // 		title: "CCC",
// // 		author: "DDD",
// // 		alreadyRead: false,
// // 		rating: 4
// // 	},
// // 	{
// // 		title: "EEE",
// // 		author: "FFF",
// // 		alreadyRead: false,
// // 		rating: 2 	
// // 	},

// // ]

// // books.forEach((books)=>{
// // 	if(books.rating>=3){
// // 		console.log(books.title);
// // 		console.log(books.rating);
// // 	}
// // })

// // for (let i = 0; i < myBooks.length; i++){
// // 	if (myBooks[i].rating >= 3)
// // 	console.log(myBooks[i].title)
// // 	console.log(myBooks[i].rating)
// // }

// //forLoop
// //  for(let i = 0; i < books.length; i++){
// // 	console.log(books[i].title)
// // }

// // books.forEach(() => console.log(book.title));
// // books.forEach((books)=>{console.log(books.title)});

// // for(let i=0; i<books.length; i++){
// // 	if(books[i].alreadyRead != true){
// // 		console.log(books[i].title);
// // 	};
// // };

// // books.forEach((books)=>{
// // 	if (books.alreadyRead === false)
// // 	console.log(books.title);
// // 	 });

// // let person = {
// // 	firstName: 'John',
// // 	lastName: 'Smith',
// // 	address:{
// // 		city: 'Tokyo',
// // 		country: 'Japan',
// // 	},
// // 	greeting: function(){
// // 		return this.address.city + " " + this.address.country;
// // 	}
	
// // };

// //hello John smith

// // function Person(){
// // 	this.firstName;
// // 	this.lastName;
// // }

// // greeting: function(){
// // 		return this.firstName + " " + this.lastName;
// // 	}

// /********************/ 
// // const myGrades = {
// // 	science: 98,
// // 	math: 87, 
// // 	english: 81, 
// // 	programming: 99,

// // }

// // console.log(myGrades)


// /*Create a method that displays the highest grade

// */

// // ASSIGNMENT


// const myGrades = {
//     science: 98,
//     math: 87,
//     english: 81,
//     programming: 99,
// }

// let grades = Object.values(myGrades);
// let max = Math.max(...grades);

// console.log(`highestGrade: ${max}`);


// const randomNum = (num) => {
// 	return Math.floor(Math.random()*num) + 1;
// }

// randomNum(3)
// result: 1 to 3

// randomNum()
// result: NaN

// const randomNum = (num) => {
// 	if(num === undefined){
// 		num = 6
// 	}
// 	return Math.floor(Math.random()*num) + 1;
// }
// // will result: 6

// Default Params:
// const randomNum = (num = 6) => {
// 	return Math.floor(Math.random()*num) + 1;
// }


/*Rest API*/ 
// https://jsonplaceholder.typicode.com/


const numbers1 = [55,66];
const numbers = [1,4,5,6,81,67];
const numbers2 = [...numbers, ...numbers1];

let name = 'JP'

const scores = {
	s1: 'P',
	s2: 'F',
	s3: 'P',
	csp1: 'P'
}

const scores2 = {
	s3: 'F',
	s4: 'P'
}


// console display:
// {...scores, ...scores2} -input
// {s1: 'P', s2: 'F', s3: 'F', csp1: 'P', s4: 'P'} -result

const info = {...scores, pased: false, id: 1245}

function response (){
	response.json
}

// console
/*
fetch('https://jsonplaceholder.typicode.com/users')
.then(function(response){
	response.json()
})

or
fetch('https://jsonplaceholder.typicode.com/users')
.then(function(response){
	return response.json()
})

fetch('https://jsonplaceholder.typicode.com/users')
.then((response) => response.json())
.then((data) => console.log(data))
*/ 

// fetch('https://jsonplaceholder.typicode.com/users') --may ibinabalik na response object
// object : methods and properties



fetch('https://jsonplaceholder.typicode.com/users')
.then()
// then run a function, get the response object and store in a parameter
// then use the response object and call its json method


function(reponse){ //object
	return response.json() //method
}

// convert to arrow function:

(response) => response.json()

// then ipasok sa .then()

fetch('https://jsonplaceholder.typicode.com/users')
.then((response) => response.json())

kung ano ang ni return dito: .then((response) => response.json()),
saluhin sa data kasi need iconsole.log

function(data){
	console.log(data)
}

// convert to arrow function:

(data) => console.log(data)

// then ipasok sa 2nd .then()
fetch('https://jsonplaceholder.typicode.com/users')
.then((response) => response.json())
.then((data) => console.log(data))



fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => console.log(data))

